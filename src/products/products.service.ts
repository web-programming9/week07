import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'Book A-Z 1', price: 100 },
  { id: 2, name: 'Book A-Z 2', price: 150 },
  { id: 3, name: 'Book A-Z 3', price: 200 },
];

let lastProdutId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProdutId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user ' + JSON.stringify(users[index]));
    // console.log('update ' + JSON.stringify(updateUserDto));
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }
  reset() {
    products = [
      { id: 1, name: 'Book A-Z 1', price: 100 },
      { id: 2, name: 'Book A-Z 2', price: 150 },
      { id: 3, name: 'Book A-Z 3', price: 200 },
    ];
    lastProdutId = 4;
    return 'RESET';
  }
}
